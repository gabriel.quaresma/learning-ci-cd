class CarsController < ActionController::API
  def index
    render json: CarsService.new.find_all
  end
end