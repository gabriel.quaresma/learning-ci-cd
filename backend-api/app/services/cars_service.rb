class CarsService
  def find_all
    [
      { model: 'Ka', brand: 'Ford' },
      { model: 'Polo', brand: 'Volkswagen' },
    ]
  end
end