# frozen_string_literal: true
require 'rails_helper'

RSpec.describe CarsController, type: :request do
  describe "GET /cars" do
    let(:cars) { double }
    let(:cars_fake) { [{"brand"=>"Ford", "model"=>"Ka"}, {"brand"=>"Volkswagen", "model"=>"Polo"}] }

    before do
      allow(CarsService).to receive(:new).and_return(cars)
      allow(cars).to receive(:find_all).and_return(cars_fake)
    end

    it "returns a list of cars" do
      get "/api/cars"

      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)).to eq(cars_fake)
    end
  end
end
