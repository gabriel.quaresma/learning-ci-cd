Rails.application.routes.draw do
  scope '/api' do
    resources :cars, only: %(index)
  end
end
